function decipher_func(key){
  code = ' key="' + key + '"; \
  a=document.getElementsByTagName("*"); \
  while (1){ \
  l=100000000; \
  for(var i=0; i<a.length;i++){ \
  if(~a[i].textContent.indexOf("$0$-")){if(a[i].textContent.length < l){l=a[i].textContent.length;j=i;}}\
  }\
  if(l === 100000000){break;}\
  str = a[j].textContent;   \
  end_code = "";\
  while(~str.indexOf("$0$")){\
    cipher = str.slice(str.indexOf("$0$-") + 4, str.indexOf("-$0$")); \
    decipher = atob(cipher);                                    \
    res = "";                                                   \
    for(var i=0; i < decipher.length; i++){                     \
    	res += String.fromCharCode(decipher.charCodeAt(i) ^ key.charCodeAt(i % key.length)); \
    }                                                           \
    end_code += str.slice(0, str.indexOf("$0$-")) + "$1$-" + res + "-$1$";\
    str = str.slice(str.indexOf("-$0$")+4, str.length);\
  }\
  end_code += str; \
  a[j].textContent = end_code;\
}'
  return(code)
}

function cipher_func(key){
  code = ' key="' + key + '";                                   \
  a=document.getElementsByTagName("*");\
  while (1){ \
  l=100000000;\
  for(var i=0; i<a.length;i++){\
  if(~a[i].textContent.indexOf("$1$-")){if(a[i].textContent.length < l){l=a[i].textContent.length;j=i;}}\
  }\
  if(l === 100000000){break;}\
  str = a[j].textContent;   \
    end_code = "";\
    while(~str.indexOf("$1$")){\
    cipher = str.slice(str.indexOf("$1$-") + 4, str.indexOf("-$1$"));\
    res = "";                                                   \
    for(var i=0; i < cipher.length; i++){                     \
    	res += String.fromCharCode(cipher.charCodeAt(i) ^ key.charCodeAt(i % key.length)); \
    }                                                           \
    end_code += str.slice(0, str.indexOf("$1$-")) + "$0$-" + btoa(res) + "-$0$";\
    str = str.slice(str.indexOf("-$1$")+4, str.length); \
  }\
  end_code += str; \
  a[j].textContent = end_code;\
}'
  return(code)
}
